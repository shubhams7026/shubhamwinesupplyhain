function addData(event) {
    event.preventDefault();
    console.log("Add data")
    const wineid= document.getElementById('wineid').value;
    const name = document.getElementById('name').value;
    const acidity = document.getElementById('acidity').value;
    const tannin = document.getElementById('tannin').value;
    const alcohol = document.getElementById('alcohol').value;
    const dom = document.getElementById('dom').value;
    console.log(wineid + name+acidity + tannin + alcohol+ dom);

    if (wineid.length == 0 || name.length == 0 || acidity.length == 0 || tannin.length == 0 || dom.length == 0 || alcohol.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/createWine', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ wineid: wineid, name: name, acidity: acidity, tannin: tannin, dom: dom, alcohol: alcohol })
        })
            .then(function (response) {
                console.log(response);
                if (response.status == 200) {
                    alert("Added a new wine");

                } else {
                    alert("Error in processing request");
                }

            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    }
}

function readData(event) {

    event.preventDefault();
    const readWineId = document.getElementById('readWineId').value;

    console.log(readWineId);

    if (readWineId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readWine', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ wineid: readWineId })
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (Winedata) {
                dataBuf = Winedata["Winedata"]
                console.log(dataBuf)
                alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    }
}