var express = require('express');
const { clientApplication } = require('../../Client/client')


var router = express.Router();
let ManufacturerClient = new clientApplication();


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Manufacturer Dashboard' });
});

router.post('/createWine', function (req, res) {

  const wineid = req.body.wineid;
  const name = req.body.name;
  const acidity = req.body.acidity;
  const tannin = req.body.tannin;
  const dom = req.body.dom;
  const alcohol = req.body.alcohol;

  ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "invokeTxn",
    "",
    "createWine",
    wineid,name, acidity, tannin, alcohol, dom
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Wine" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/readWine', async function (req, res) {
  const readWineId = req.body.wineid;

  ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "queryTxn",
    "",
    "readWine",
    readWineId
  ).then(message => {
    console.log(message.toString());
    res.send({ Winedata: message.toString() });
  });

})



module.exports = router;