#!/bin/sh

echo "Start the network"
minifab netup -s couchdb -e true -o manufacturer.wine.com

sleep 5

echo "Create the channel"
minifab create -c winechannel
 
sleep 2

echo "Join the peers to channel"
minifab join -c winechannel

sleep 2

echo "Anchor update"
minifab anchorupdate

sleep 2

echo "Generate the required materials"
minifab profilegen -c winechannel
