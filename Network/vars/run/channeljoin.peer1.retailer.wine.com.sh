#!/bin/bash
# Script to join a peer to a channel
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.68.166:7005
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/retailer.wine.com/peers/peer1.retailer.wine.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=retailer-wine-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/retailer.wine.com/users/Admin@retailer.wine.com/msp
export ORDERER_ADDRESS=192.168.68.166:7011
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/wine.com/orderers/orderer3.wine.com/tls/ca.crt
if [ ! -f "winechannel.genesis.block" ]; then
  peer channel fetch oldest -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA \
  --tls -c winechannel /vars/winechannel.genesis.block
fi

peer channel join -b /vars/winechannel.genesis.block \
  -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA --tls
