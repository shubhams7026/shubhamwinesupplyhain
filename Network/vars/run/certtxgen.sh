#!/bin/bash
cd $FABRIC_CFG_PATH
# cryptogen generate --config crypto-config.yaml --output keyfiles
configtxgen -profile OrdererGenesis -outputBlock genesis.block -channelID systemchannel

configtxgen -printOrg dealer-wine-com > JoinRequest_dealer-wine-com.json
configtxgen -printOrg manufacturer-wine-com > JoinRequest_manufacturer-wine-com.json
configtxgen -printOrg retailer-wine-com > JoinRequest_retailer-wine-com.json
