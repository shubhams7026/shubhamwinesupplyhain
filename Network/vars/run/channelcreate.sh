#!/bin/bash
# Script to create channel block 0 and then create channel
cp $FABRIC_CFG_PATH/core.yaml /vars/core.yaml
cd /vars
export FABRIC_CFG_PATH=/vars
configtxgen -profile OrgChannel \
  -outputCreateChannelTx winechannel.tx -channelID winechannel

export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.68.166:7003
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/manufacturer.wine.com/peers/peer1.manufacturer.wine.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=manufacturer-wine-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/manufacturer.wine.com/users/Admin@manufacturer.wine.com/msp
export ORDERER_ADDRESS=192.168.68.166:7011
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/wine.com/orderers/orderer3.wine.com/tls/ca.crt
peer channel create -c winechannel -f winechannel.tx -o $ORDERER_ADDRESS \
  --cafile $ORDERER_TLS_CA --tls
