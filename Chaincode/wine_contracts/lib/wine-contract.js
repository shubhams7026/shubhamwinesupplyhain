/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class WineContract extends Contract {

    async wineExists(ctx, wineId) {
        const buffer = await ctx.stub.getState(wineId);
        return (!!buffer && buffer.length > 0);
    }

    async createWine(ctx, wineId,name, acidity, tannin, alcohol, dateOfManufacture) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-wine-com') {

        const exists = await this.wineExists(ctx, wineId);
        if (exists) {
            throw new Error(`The wine ${wineId} already exists`);
        }
        const asset = { 
            name,
            acidity,
            tannin, 
            alcohol, 
            dateOfManufacture,
            status: 'In Factory',
            assetType: 'Wine',


         };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(wineId, buffer);
        let addWineEventData = { Type: 'Wine creation', alcohol:alcohol };
        await ctx.stub.setEvent('addWineEvent', Buffer.from(JSON.stringify(addWineEventData)));

    }
    else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}

    async readWine(ctx, wineId) {
        const exists = await this.wineExists(ctx, wineId);
        if (!exists) {
            throw new Error(`The wine ${wineId} does not exist`);
        }
        const buffer = await ctx.stub.getState(wineId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateWine(ctx, wineId, newValue) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-wine-com') {
        const exists = await this.wineExists(ctx, wineId);
        if (!exists) {
            throw new Error(`The wine ${wineId} does not exist`);
        }
        const asset = { value: newValue };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(wineId, buffer);
    }else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}

    async deleteWine(ctx, wineId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-wine-com') {
        const exists = await this.wineExists(ctx, wineId);
        if (!exists) {
            throw new Error(`The wine ${wineId} does not exist`);
        }
        await ctx.stub.deleteState(wineId);
    }else{
        return `User under following MSP:${mspID} cannot able to perform this action`;
    }
}
async queryAllWine(ctx) {
    const queryString = {
        selector: {
            assetType: 'Wine',

        },
        // sort: [{ dateOfManufacture: "asc" }],
    };
    let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
    let result = await this.getAllResults(resultIterator, false)
    return JSON.stringify(result);
}

async getWineHistory(ctx, wineId) {
    let resultIterator = await ctx.stub.getHistoryForKey(wineId);
    let result = await this.getAllResults(resultIterator, true)
    return JSON.stringify(result);
}

async getWineByRange(ctx, startkey, endkey) {
    let resultIterator = await ctx.stub.getStateByRange(startkey, endkey);
    let result = await this.getAllResults(resultIterator, false)
    return JSON.stringify(result);

}
async getAllResults(iterator, isHistory) {
    let allResult = []
    for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
        if (res.value && res.value.value.toString()) {
            let jsonRes = {}
            if (isHistory && isHistory == true) {
                jsonRes.TxId = res.value.txId;
                jsonRes.timestamp = res.value.timestamp;
                jsonRes.Value = JSON.parse(res.value.value.toString());

            }
            else {
                jsonRes.Key = res.value.key
                jsonRes.Record = JSON.parse(res.value.value.toString())
            }
            allResult.push(jsonRes)
        }
    }
    await iterator.close()
    return allResult;

}


}

module.exports = WineContract;