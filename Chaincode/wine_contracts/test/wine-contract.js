/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { WineContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('WineContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new WineContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"wine 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"wine 1002 value"}'));
    });

    describe('#wineExists', () => {

        it('should return true for a wine', async () => {
            await contract.wineExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a wine that does not exist', async () => {
            await contract.wineExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createWine', () => {

        it('should create a wine', async () => {
            await contract.createWine(ctx, '1003', 'wine 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"wine 1003 value"}'));
        });

        it('should throw an error for a wine that already exists', async () => {
            await contract.createWine(ctx, '1001', 'myvalue').should.be.rejectedWith(/The wine 1001 already exists/);
        });

    });

    describe('#readWine', () => {

        it('should return a wine', async () => {
            await contract.readWine(ctx, '1001').should.eventually.deep.equal({ value: 'wine 1001 value' });
        });

        it('should throw an error for a wine that does not exist', async () => {
            await contract.readWine(ctx, '1003').should.be.rejectedWith(/The wine 1003 does not exist/);
        });

    });

    describe('#updateWine', () => {

        it('should update a wine', async () => {
            await contract.updateWine(ctx, '1001', 'wine 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"wine 1001 new value"}'));
        });

        it('should throw an error for a wine that does not exist', async () => {
            await contract.updateWine(ctx, '1003', 'wine 1003 new value').should.be.rejectedWith(/The wine 1003 does not exist/);
        });

    });

    describe('#deleteWine', () => {

        it('should delete a wine', async () => {
            await contract.deleteWine(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a wine that does not exist', async () => {
            await contract.deleteWine(ctx, '1003').should.be.rejectedWith(/The wine 1003 does not exist/);
        });

    });

});
