/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const WineContract = require('./lib/wine-contract');
const OrderContract = require('./lib/order-contract');

module.exports.WineContract = WineContract;
module.exports.OrderContract = OrderContract;
module.exports.contracts = [ WineContract,OrderContract ];
