const { EventListener } = require('./event')

let ManufacturerEvent = new EventListener();
ManufacturerEvent.contractEventListener("manufacturer", "Admin", "winechannel",
    "wine_contracts", "WineContract", "addWineEvent");