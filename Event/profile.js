let profile = {
    manufacturer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/manufacturer.wine.com",
        "CP": "../Network/vars/profiles/winechannel_connection_for_nodesdk.json"
    },
    dealer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/dealer.wine.com",
        "CP": "../Network/vars/profiles/winechannel_connection_for_nodesdk.json"
    }
}
module.exports = { profile }