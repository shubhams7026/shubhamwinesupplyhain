
function toManuDash() {
    window.location.href='/manufacturer';
}

function swalBasic(data) {
    swal.fire({
        // toast: true,
        icon: `${data.icon}`,
        title: `${data.title}`,
        animation: true,
        position: 'center',
        showConfirmButton: true,
        footer: `${data.footer}`,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
        }
    })
}

// function swalDisplay(data) {
//     swal.fire({
//         // toast: true,
//         icon: `${data.icon}`,
//         title: `${data.title}`,
//         animation: false,
//         position: 'center',
//         html: `<h3>${JSON.stringify(data.response)}</h3>`,
//         showConfirmButton: true,
//         timer: 3000,
//         timerProgressBar: true,
//         didOpen: (toast) => {
//             toast.addEventListener('mouseenter', swal.stopTimer)
//             toast.addEventListener('mouseleave', swal.resumeTimer)
//         }
//     }) 
// }

function reloadWindow() {
    window.location.reload();
}

function ManWriteData(){
    event.preventDefault();
    const wineid = document.getElementById('wineid').value;
    const name = document.getElementById('name').value;
    const acidity = document.getElementById('acidity').value;
    const tannin = document.getElementById('tannin').value;
    const dateOfManufacture = document.getElementById('dateOfManufacture').value;
    const alcohol = document.getElementById('alcohol').value;
    console.log(wineid,name,acidity,tannin,dateOfManufacture,alcohol);

    if (wineid.length==0||name.length==0||acidity.length==0||tannin.length==0||dateOfManufacture.length==0||alcohol.length==0) {
        const data = {
            title: "You might have missed something",
            footer: "Enter all mandatory fields to add a new car",
            icon: "warning"
        }
        swalBasic(data);
        }
    else{
        fetch('/manuwrite',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({wineid: wineid, name: name, acidity: acidity, tannin: tannin, dateOfManufacture: dateOfManufacture, alcohol: alcohol})
        })
        .then(function(response){
            if(response.status == 200) {
                const data = {
                    title: "Success",
                    footer: "Added a new wine",
                    icon: "success"
                }
                swalBasic(data);
            } else {
                const data = {
                    title: `wine with wine ${wineid} already exists`,
                    footer: "wineid must be unique",
                    icon: "error"
                }
                swalBasic(data);
            }

        })
        .catch(function(error){
            const data = {
                title: "Error in processing Request",
                footer: "Something went wrong !",
                icon: "error"
            }
            swalBasic(data);
        })    
    }
}
function ManQueryData(){

    event.preventDefault();
    const wineid = document.getElementById('wineid').value;
    
    console.log(wineid);

    if (wineid.length==0) {
        const data = {
            title: "Enter a Valid  wineID",
            footer: "This is a mandatory field",
            icon: "warning"
        }
        swalBasic(data)  
    }
    else{
        fetch('/manuread',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',              
            },
            body: JSON.stringify({wineid: wineid})
        })
        .then(function(response){
            console.log(response);
            return response.json();
        })
        .then(function (winedata){
            dataBuf = winedata["winedata"]
            swal.fire({
                // toast: true,
                icon: `success`,
                title: `Current status of wine with wineid ${wineid} :`,
                animation: false,
                position: 'center',
                html: `<h3>${dataBuf}</h3>`,
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            }) 
        })
        .catch(function(error){
            const data = {
                title: "Error in processing Request",
                footer: "Something went wrong !",
                icon: "error"
            }
            swalBasic(data);        
        })    
    }
}

//Method to get the history of an item
function getItemHistory(wineid) {
    console.log("postalId", wineid)
    window.location.href = '/itemhistory?wineid=' + wineid;
}

function getMatchingOrders(wineid) {
    console.log("wineid",wineid)
    window.location.href = 'matchOrder?wine=' + wineid;
}

// function RegisterWine(){
//     console.log("Entered the register function")
//     event.preventDefault();
//     const wineid = document.getElementById('wineid').value;
   
//     const regNumber = document.getElementById('regNumber').value;
//     console.log(QVinNumb+carOwner+regNumber);

//     if (wineid.length==0||regNumber.length==0) {
//         const data = {
//             title: "You have missed something",
//             footer: "All fields are mandatory",
//             icon: "warning"
//         }
//         swalBasic(data)   
//     }
//     else{
//         fetch('/registerWine',{
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',              
//             },
//             body: JSON.stringify({wineid: wineid, regNumber: regNumber})
//         })
//         .then(function(response){
//             if(response.status === 200){
//             const data = {
//                 title: `Registered wine ${wineid} to buyer`,
//                 footer: "Registered wine",
//                 icon: "success"
//             }
//             swalBasic(data)
//             } else {
//                 const data = {
//                     title: `Failed to register wine`,
//                     footer: "Please try again !!",
//                     icon: "error"
//                 }
//                 swalBasic(data)           
//             }
//         })
//         .catch(function(err){
//             const data = {
//                 title: "Error in processing Request",
//                 footer: "Something went wrong !",
//                 icon: "error"
//             }
//             swalBasic(data);         
//         })    
//     }
// }

function createOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderid = document.getElementById('orderid').value;
    const tannin = document.getElementById('tannin').value;
    console.log(orderid ,tannin );

    if (orderid.length == 0 || tannin.length == 0) {
            const data = {
                title: "You have missed something",
                footer: "All fields are mandatory",
                icon: "warning"
            }
            swalBasic(data)  
    }
    else {
        fetch('/createOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderid: orderid, tannin: tannin})
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order is created`,
                        footer: "Raised Order",
                        icon: "success"
                    }
                    swalBasic(data)

                } else {
                    const data = {
                        title: `Failed to create order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)                  }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);               
            })
    }
}

function readOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderid = document.getElementById('orderid').value;
    
    console.log(orderid);

    if (orderid.length == 0) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)     
    }
    else {
        fetch('/readOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderid: orderid})
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (orderdata){
                dataBuf = orderdata["orderdata"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Order : `,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })           
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);              
            })
    }
}

// function matchOrder(orderId,carId) {
//     if (!orderId|| !carId) {
//         const data = {
//             title: "Enter a order number",
//             footer: "Order Number is mandatory",
//             icon: "warning"
//         }
//         swalBasic(data)   
//     } else {
//         fetch('/match', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//             body: JSON.stringify({ orderId,carId})
//         })
//             .then(function (response) {
//                 if (response.status === 200) {
//                     const data = {
//                         title: `Order matched successfully`,
//                         footer: "Order matched",
//                         icon: "success"
//                     }
//                     swalBasic(data)
//                 } else {
//                     const data = {
//                         title: `Failed to match order`,
//                         footer: "Please try again !!",
//                         icon: "error"
//                     }
//                     swalBasic(data)                 }
//             })
            
//             .catch(function (err) {
//                 const data = {
//                     title: "Error in processing Request",
//                     footer: "Something went wrong !",
//                     icon: "error"
//                 }
//                 swalBasic(data);  
//             })
//     }
// }


function allOrders() {
    window.location.href='/allOrders';
}


function getEvent() {
    fetch('/event', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(function (response) {
            console.log("Response is ###",response)
            return response.json()
        })
        .then(function (event) {
            dataBuf = event["wineEvent"]
            swal.fire({
                toast: true,
                // icon: `${data.icon}`,
                title: `Event : `,
                animation: false,
                position: 'top-right',
                html: `<h5>${dataBuf}</h5>`,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            }) 
        })
        .catch(function (err) {
            swal.fire({
                toast: true,
                icon: `error`,
                title: `Error`,
                animation: false,
                position: 'top-right',
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })        
        })
}