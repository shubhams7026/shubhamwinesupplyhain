var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client');
const {Events} = require('./events')
let eventClient = new Events()
eventClient.contractEventListner("manufacturer", "Admin", "winechannel",
"wine_contracts", "WineContract", "addWineEvent")



/* GET home page. */
router.get('/', function(req, res, next) {
  let retailerClient = new clientApplication();
 
  retailerClient.generatedAndEvaluateTxn(
      "retailer",
      "Admin",
      "winechannel", 
      "wine_contracts",
      "WineContract",
      "queryAllWine"
  )
  .then(wine => {
    const dataBuffer = wine.toString();
    console.log("wines are ", wine.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('index', { title: 'Wine shoppe', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })
});
 
router.get('/manufacturer', function(req, res, next) {
  let manufacturerClient = new clientApplication();
  manufacturerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "queryAllWine"
  ).then(wine =>{
    const data =wine.toString();
    const value = JSON.parse(data)
    res.render('manufacturer', { title: 'Manufacturer Dashboard', itemList: value });
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })

});
router.get('/dealer', function(req, res, next) {
  res.render('dealer', { title: 'Dealer Dashboard' });
});

router.get('/event', function(req, res, next) {
  console.log("Event Response %%%$$^^$%%$",eventClient.getEvents().toString())
  var event = eventClient.getEvents().toString()
  res.send({wineEvent: event})
  // .then(array => {
  //   console.log("Value is #####", array)
  //   res.send(array);
  // }).catch(err => {
  //   console.log("errors are ", err)
  //   res.send(err)
  // })
  // res.render('index', { title: 'Dealer Dashboard' });
});


router.get('/retailer', function(req, res, next) {
  let retailerClient = new clientApplication();
  retailerClient.generatedAndEvaluateTxn(
    "retailer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "queryAllWine"
  )
  .then(wine => {
    const dataBuffer = wine.toString();
    console.log("wines are ", wine.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('retailer', { title: 'Retailer Dashboard', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
})});



router.get('/addWineEvent', async function(req, res, next) {
  let retailerClient = new clientApplication();
  const result = await retailerClient.contractEventListner("manufacturer", "Admin", "winechannel", 
  "wine_contracts", "addWineEvent")
  console.log("The result is ####$$$$",result)
  res.render('manufacturer', {view: "wineEvents", results: result })
})

router.post('/manuwrite',function(req,res){

  const wineid = req.body.wineid;
  const name = req.body.name;
  const acidity = req.body.acidity;
  const tannin = req.body.tannin;
  const dom = req.body.dom;
  const alcohol = req.body.alcohol;

  // console.log("Request Object",req)
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndSubmitTxn(
      "manufacturer",
      "Admin",
      "winechannel",
      "wine_contracts",
      "WineContract",
      "createWine",
      wineid,name,acidity,tannin,dom,alcohol
    ).then(message => {
        console.log("Message is $$$$",message)
        res.status(200).send({message: "Added Wine"})
      }
    )
    .catch(error =>{
      console.log("Some error Occured $$$$###", error)
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });
});

router.post('/manuread',async function(req,res){
  const wineid = req.body.wineid;
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "readWine", wineid)
    .then(message => {
      
      res.status(200).send({ winedata : message.toString() });
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });

 })

 //  Get History of a car
 router.get('/itemhistory',async function(req,res){
  const wineid = req.query.wineid;
 
  let retailerClient = new clientApplication();
  
  retailerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "getWineHistory", wineid).then(message => {
    const dataBuffer = message.toString();
    
    const value = JSON.parse(dataBuffer)
    res.render('history', { itemList: value , title: "wine History"})

  });

 })

//  Register a car

//  router.post('/registerWine',async function(req,res){
//   const Qvin = req.body.wineid;
//   const RegistrationNumber = req.body.regNumber
//   let MVDClient = new clientApplication();
  
//   MVDClient.generatedAndSubmitTxn( 
//     "mvd",
//     "Admin",
//     "autochannel", 
//     "KBA-Automobile",
//     "CarContract",
//     "registerCar", wineid,CarOwner,RegistrationNumber)
//     .then(message => {
    
//       res.status(200).send("Successfully created")
//     }).catch(error =>{
     
//       res.status(500).send({error:`Failed to create`,message:`${error}`})
//     });

//  })
// Create order
router.post('/createOrder',async function(req,res){
  const orderid = req.body.orderid;
  const tannin = req.body.tannin;
 
  let DealerClient = new clientApplication();

  const transientData = {
    tannin: Buffer.from(tannin)
 
  }
  
  DealerClient.generatedAndSubmitPDC( 
    "dealer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "OrderContract",
    "createOrder", orderid,transientData)
    .then(message => {
      
      res.status(200).send("Successfully created")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to create`,message:`${error}`})
    });

 })

 router.post('/readOrder',async function(req,res){
  const orderNumber = req.body.orderNumber;
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "winechannel", 
    "wine_contracts",
    "OrderContract",
    "readOrder", orderNumber).then(message => {
   
    res.send({orderData : message.toString()});
  }).catch(error => {
    alert('Error occured')
  })

 })

 //Get all orders
 router.get('/allOrders',async function(req,res){
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "OrderContract",
    "queryAllOrders","").then(message => {
    const dataBuffer = message.toString();
    const value = JSON.parse(dataBuffer);
    res.render('orders', { itemList: value , title: "All Orders"})
    }).catch(error => {
    //alert('Error occured')
    console.log(error)
  })

 })
 //Find matching orders
//  router.get('/matchOrder',async function(req,res){
//   const carId = req.query.carId;
 
//   let mvdClient = new clientApplication();
  
//   mvdClient.generatedAndEvaluateTxn( 
//     "manufacturer",
//     "Admin",
//     "autochannel", 
//     "KBA-Automobile",
//     "CarContract",
//     "checkMatchingOrders", carId).then(message => {
//     console.log("Message response",message)
//     var dataBuffer = message.toString();
//     var data =[];
//     data.push(dataBuffer,carId)
//     console.log("checkMatchingOrders",data)
//     const value = JSON.parse(dataBuffer)
//     let array = [];
//     if(value.length) {
//         for (i = 0; i < value.length; i++) {
//             array.push({
//                "orderId": `${value[i].Key}`,"carId":`${carId}`,
//                 "Make": `${value[i].Record.make}`, "Model":`${value[i].Record.model}`, 
//                 "Color":`${value[i].Record.color}`, 
//                 "dealerName": `${value[i].Record.dealerName}`,"assetType": `${value[i].Record.assetType}`
//             })
//         }
//     }
//     console.log("Array value is ", array)
//     console.log("Car id sent",carId)
//     res.render('matchOrder', { itemList: array , title: "Matching Orders"})

//   });

//  })

//  router.post('/match',async function(req,res){
//   const orderId = req.body.orderId;
//   const carId = req.body.carId
//   let DealerClient = new clientApplication();
//   DealerClient.generatedAndSubmitTxn( 
//     "dealer",
//     "Admin",
//     "autochannel", 
//     "KBA-Automobile",
//     "CarContract",
//     "matchOrder", carId,orderId).then(message => {
   
//       res.status(200).send("Successfully Matched order")
//     }).catch(error =>{
     
//       res.status(500).send({error:`Failed to Match Order`,message:`${error}`})
//     });

//  })



module.exports = router;


