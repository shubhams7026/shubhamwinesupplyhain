let profile = {
    dealer: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/dealer.wine.com",
        "CP": "../Network/vars/profiles/winechannel_connection_for_nodesdk.json"
    },
    retailer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/retailer.wine.com",
        "CP": "../Network/vars/profiles/winechannel_connection_for_nodesdk.json"
    },
    manufacturer:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/manufacturer.wine.com",
        "CP": "../Network/vars/profiles/winechannel_connection_for_nodesdk.json"
    }


} 

module.exports = {
    profile
}
