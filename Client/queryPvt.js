const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "OrderContract",
    "queryTxn",
    "",
    "readOrder",
    "3"
).then(message => {
    console.log(message.toString())
})