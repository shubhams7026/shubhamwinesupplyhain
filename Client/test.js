const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication();

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "invokeTxn",
    "",
    "createWine",
    "103",
    "10%",
    "10%",
    "10%",
    "10%",
    "27/08/1999"
).then(message => {
    console.log(message.toString());
})