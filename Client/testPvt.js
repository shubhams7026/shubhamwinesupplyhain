const { clientApplication } = require('./client');

let dealerClient = new clientApplication();
const transientData = {
   tannin: Buffer.from('10%'),

}

dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "3"
).then(msg => {
    console.log(msg.toString())
});