const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "winechannel",
    "wine_contracts",
    "WineContract",
    "queryTxn",
    "",
    "readWine",
    "103"
).then(message => {
    console.log(message.toString())
})